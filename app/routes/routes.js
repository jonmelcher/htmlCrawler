const crawl = require('../crawler/crawl');

const appRouter = function(app) {
    app.post('/', async function(req, res) {
        const response = await crawl(req.body.url, req.body.tags);
        res.send(response);
    });
};

module.exports = appRouter;
