function parse(name, tag) {

    var nodes = Array.prototype.slice.call(document.querySelectorAll(name));

    var parseNone = function(required) {
        return nodes.reduce(function(acc, node) {
            for (var i = 0; i < required.length; ++i) {
                if (node.getAttribute(required[i].key) === required[i].value) {
                    return acc;
                }
            }
            return acc.concat({
                primary: name,
                secondary: null,
                value: node.getAttribute(tag.attribute) || node.innerText
            });
        }, []);
    };

    var parseFirst = function(required) {
        for (var j = 0; j < nodes.length; ++j) {
            for (var i = 0; i < required.length; ++i) {
                if (nodes[j].getAttribute(required[i].key) === required[i].value) {
                    return [{
                        primary: name,
                        secondary: required[i].value,
                        value: nodes[j].getAttribute(tag.attribute)
                    }];
                }
            }
        }
        return [];
    };

    var parseAll = function(required) {
        return nodes.reduce(function(acc, node) {
            for (var i = 0; i < required.length; ++i) {
                if (node.getAttribute(required[i].key) !== required[i].value) {
                    return acc;
                }
            }
            return acc.concat({
                primary: name,
                secondary: null,
                value: node.getAttribute(tag.attribute) || node.innerText
            });
        }, []);
    };

    var strategy = {
        'none': parseNone,
        'first': parseFirst,
        'all': parseAll
    };

    return strategy[tag.predicate](tag.required);
}

module.exports = parse;
