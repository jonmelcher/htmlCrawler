const express = require("express");
const bodyParser = require("body-parser");


const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const routes = require("./routes/routes.js")(app);

const server = app.listen(8080, function() {
    console.log("Listening on port %s...", server.address().port);
});
