const phantom = require('phantom');
const parse = require('./parse');


async function crawl(url, tags) {
    const instance = await phantom.create();
    const page = await instance.createPage();
    const status = await page.open(url);
    const parsed = { results: [] };
    await Object.keys(tags).forEach(async function(key) {
        const parsedTag = await page.evaluate(parse, key, tags[key]);
        parsed.results = parsed.results.concat(parsedTag);
    });
    await instance.exit();
    return parsed;
};

module.exports = crawl;
